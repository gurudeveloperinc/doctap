<?php

namespace App\Http\Controllers\Auth;

use App\hospital;
use App\lab;
use App\pharmacy;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/register';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'fname' => 'required|string|max:255',
            'sname' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

	public function register(Request $request)
	{

		if($request->hasFile('image')){
			$filename = $request->file('image')->getClientOriginalName();
			$request->file('image')->move('profileImages', $filename);

			$profileUrl = url('/profileImages/' . $filename);

		} else {
			$profileUrl = url('/profileImages/default-image.png');

		}

		$this->validator($request->all())->validate();

		event(new Registered($user = $this->create($request->all(), $profileUrl)));

		$request->session()->flash('success','Registration successful');

		return $this->registered($request, $user)
			?: redirect($this->redirectPath());
	}


	protected function create(array $data, $profileUrl)
    {

    	$user = User::create([
		    'fname' => $data['fname'],
		    'sname' => $data['sname'],
		    'role' => $data['role'],
		    'email' => $data['email'],
		    'phone' => $data['phone'],
		    'address' => $data['address'],
		    'image' => $profileUrl,
		    'password' => bcrypt($data['password']),
	    ]);

    	switch($data['role']){
		    case 'Pharmacy':
		    	$pharmacy = new pharmacy();
		    	$pharmacy->name = $data['placename'];
		    	$pharmacy->address = $data['placeaddress'];
		    	$pharmacy->city = $data['placecity'];
		    	$pharmacy->uid = $user->uid;
		    	$pharmacy->save();
		    	break;
		    case 'Lab':
			    $lab = new lab();
			    $lab->name = $data['placename'];
			    $lab->address = $data['placeaddress'];
			    $lab->city = $data['placecity'];
			    $lab->uid = $user->uid;
			    $lab->save();

		    	break;
		    case 'Hospital':
			    $hospital = new hospital();
			    $hospital->name = $data['placename'];
			    $hospital->address = $data['placeaddress'];
			    $hospital->city = $data['placecity'];
			    $hospital->uid = $user->uid;
			    $hospital->save();
		    	break;
	    }

	    return $user;

    }
}
