<?php

namespace App\Http\Controllers;

use App\cases;
use App\caseTest;
use App\doctor;
use App\hospital;
use App\lab;
use App\patient;
use App\payment;
use App\pharmacy;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class LabController extends Controller
{

	public function __construct() {

		$this->middleware('auth');
	}

	public function dashboard() {

		$caseTests = caseTest::where('labid',Auth::user()->Lab->labid)->get();
		$caseTestsArray = array();
		foreach($caseTests as $caseTest){
			array_push($caseTestsArray,$caseTest->casid);
		}

		$cases = cases::whereIn('casid',$caseTestsArray)->get();
		$pendingCases = $cases->where('status','Pending');

		$hospitals = hospital::all();
		$hospitalsToday = hospital::where('created_at','>',Carbon::today())->get();

		$labs = lab::all();
		$labsToday = lab::where('created_at','>',Carbon::today())->get();

		$pharmacies = pharmacy::all();
		$pharmaciesToday = pharmacy::where('created_at','>',Carbon::today())->get();

		$doctors = doctor::all();
		$doctorsToday = doctor::where('created_at','>=',Carbon::today())->get();

		$payments = payment::all();
		$paymentsToday = payment::where('created_at','>',Carbon::today())->get();

		$patients = patient::all();
		$patientsToday = patient::where('created_at','>',Carbon::today())->get();

		return view('labview.dashboard',[
			'cases' => $cases,
			'pendingCases' => $pendingCases,

			'hospitals' =>$hospitals,
			'hospitalsToday' =>$hospitalsToday,

			'labs' => $labs,
			'labsToday' => $labsToday,

			'pharmacies' => $pharmacies,
			'pharmaciesToday' => $pharmaciesToday,

			'doctors' => $doctors,
			'doctorsToday' => $doctorsToday,

			'patients' => $patients,
			'patientsToday' => $patientsToday,

			'payments' => $payments,
			'paymentsToday' => $paymentsToday,

		]);

	}

	public function cases() {

		$caseTests = caseTest::where('labid',Auth::user()->Lab->labid)->get();
		$caseTestsArray = array();
		foreach($caseTests as $caseTest){
			array_push($caseTestsArray,$caseTest->casid);
		}

		$cases = cases::whereIn('casid',$caseTestsArray)->get();


		return view('labview.cases',[
			'cases' => $cases
		]);
	}

	public function patients() {

		$caseTests = caseTest::where('labid',Auth::user()->Lab->labid)->get();
		$caseTestsArray = array();
		foreach($caseTests as $caseTest){
			array_push($caseTestsArray,$caseTest->casid);
		}

		$cases = cases::whereIn('casid',$caseTestsArray)->get();

		foreach($cases as $item){
		return	$item->Patient;
		}



	}
}
