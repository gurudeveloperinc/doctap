<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class caseDoctor extends Model
{
    protected $primaryKey = 'cdid';
    protected $table = 'casedoctors';


	public function Case() {
		return $this->belongsTo(cases::class,'casid','casid');
    }

	public function Doctor() {
		return $this->belongsTo(doctor::class,'docid','docid');
    }
}
