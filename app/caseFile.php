<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class caseFile extends Model
{
    protected $primaryKey = 'cfid';
    protected $table = 'casefiles';
}
