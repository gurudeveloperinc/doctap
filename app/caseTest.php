<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class caseTest extends Model
{
    protected $primaryKey = 'ctid';
    protected $table = 'casetests';

	public function Case() {
		return $this->belongsTo(cases::class,'casid','casid');
    }
	public function Test() {
		return $this->belongsTo(test::class,'testid','testid');
    }

	public function Doctor() {
		return $this->belongsTo(doctor::class,'docid','docid');
    }
}
