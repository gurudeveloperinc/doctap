<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class caseUpdate extends Model
{
    protected $primaryKey = 'cuid';
    protected $table = 'caseupdates';


	public function Case() {
		return $this->belongsTo(cases::class,'casid','casid');
    }

	public function Doctor() {
		return $this->belongsTo(doctor::class,'docid','docid');
    }
}
