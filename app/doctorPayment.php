<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class doctorPayment extends Model
{
    protected $primaryKey = 'dcpid';
    protected $table = 'docpayments';

	public function Doctor() {
		return $this->belongsTo(doctor::class,'docid','docid');
    }

}
