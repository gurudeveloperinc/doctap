<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class doctorsDoc extends Model
{
    protected $primaryKey = 'dcid';
    protected $table = 'doctor_docs';
}
