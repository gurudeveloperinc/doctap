<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class feedback extends Model
{
    protected $primaryKey = 'fdid';
    protected $table      = 'feedback';

	public function Patient() {
		return $this->belongsTo(patient::class,'patid','patid');
    }

	public function Case() {
		return $this->belongsTo(cases::class,'casid','casid');
    }

	public function User() {
		return $this->belongsTo(User::class,'uid','uid');
    }
}
