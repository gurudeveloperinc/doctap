<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class hospital extends Model
{
    protected $primaryKey = 'hid';
    protected $table = 'hospitals';

	public function User() {
		return $this->belongsTo(User::class,'uid','uid');
    }

	public function CreatedBy() {
		return $this->belongsTo(User::class,'createdBy','uid');
	}

	public function Specialization() {
		return $this->hasMany(hospitalSpec::class,'hid','hid');
	}


}
