<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class hospitalSpec extends Model
{
	protected $primaryKey = 'hsid';
	protected $table = 'hospital_specializations';

	public function Specialization() {
		return $this->belongsTo(specialization::class,'spid' ,'spid');
	}

}
