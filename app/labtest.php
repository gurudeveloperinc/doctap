<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class labtest extends Model
{
    protected $primaryKey = 'ltid';
    protected $table = 'lab_tests';

	public function Test() {
		return $this->belongsTo(test::class,'testid','testid');
    }
}
