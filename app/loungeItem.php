<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class loungeItem extends Model
{
    protected $primaryKey = 'lid';
    protected $table      = 'lounge';

	public function User() {
		return $this->belongsTo(User::class,'uid','uid');
    }
}
