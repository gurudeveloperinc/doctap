<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class patient extends Model
{
    protected $primaryKey = 'patid';
    protected $table = 'patients';
    protected $guarded = [];
    protected $hidden = ['password','authCode'];

	public function Dependants() {
		return $this->hasMany(patientDependant::class,'patid','patid');
    }

	public function Cases() {
		return $this->hasMany(cases::class,'patid','patid');
    }

	public function History() {
		return $this->hasMany(medicalHistory::class,'patid','patid')->where('type',"=","patient");
    }

}
