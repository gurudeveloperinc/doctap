<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class patientDependant extends Model
{
	protected $primaryKey = 'pdid';
	protected $table = 'patient_dependants';
	protected $guarded = [];

	public function Patient() {
		return $this->belongsTo(patient::class,'patid','patid');
    }

	public function Cases() {
		return $this->hasMany(cases::class,'pdid','pdid');
    }

	public function History() {
		return $this->hasMany(medicalHistory::class,'pdid','pdid');
    }
}
