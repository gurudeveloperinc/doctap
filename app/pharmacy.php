<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pharmacy extends Model
{
    protected $primaryKey = 'pharmid';
    protected $table = 'pharmacies';

	public function Cases() {
		return $this->hasMany(cases::class,'pharmid','pharmid');
    }

	public function User() {
		return $this->belongsTo(User::class,'uid','uid');
	}

	public function CreatedBy() {
		return $this->belongsTo(User::class,'createdBy','uid');
	}

}
