<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class prescription extends Model
{
    protected $primaryKey = 'prid';
    protected $table = 'prescriptions';

	public function Doctor() {
		return $this->hasOne(doctor::class,'docid','docid');
    }

	public function Case() {
		return $this->hasOne(cases::class,'casid','casid');
    }
}
