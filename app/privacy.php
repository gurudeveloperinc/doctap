<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class privacy extends Model
{
    protected $table = 'privacy';
    protected $primaryKey = 'pid';
	protected $hidden = ['deleted_at'];
}
