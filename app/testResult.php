<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class testResult extends Model
{
    protected $primaryKey = 'trid';
    protected $table = 'testresults';

	public function CaseTest() {
		return $this->belongsTo(caseTest::class,'ctid','ctid');
    }
}
