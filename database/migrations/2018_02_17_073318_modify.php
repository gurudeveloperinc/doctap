<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Modify extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

	    Schema::create('settings', function(Blueprint $table){
		    $table->increments('setid');
		    $table->string('name');
		    $table->string('value');
		    $table->timestamps();

	    });

	    Schema::create('lab_tests', function(Blueprint $table){
		    $table->increments('ltid');
		    $table->integer('labid');
		    $table->integer('testid');
		    $table->timestamps();
	    });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
