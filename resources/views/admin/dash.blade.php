<?php use Carbon\Carbon; ?>
@extends('layouts.admin')

@section('content')

    <div class="page-content">

        <div class="content sm-gutter" style="padding-top: 40px;">
            <div class="page-title">
            </div>

            <script>
                $(document).ready(function () {
                    $('.casesDiv').click(function () {
                        window.location = '{{url('manage-cases')}}';
                    });
                    $('#patientsDiv').click(function () {
                        window.location = '{{url('manage-patients')}}';
                    });
                    $('#doctorsDiv').click(function () {
                        window.location = '{{url('manage-doctors')}}';
                    });
                    $('#revenueDiv').click(function () {
                        window.location = '{{url('manage-payments')}}';
                    });
                    $('#hospitalsDiv').click(function () {
                        window.location = '{{url('manage-hospitals')}}';
                    });
                    $('#pharmaciesDiv').click(function () {
                        window.location = '{{url('manage-pharmacies')}}';
                    });
                    $('#labsDiv').click(function () {
                        window.location = '{{url('manage-labs')}}';
                    });
                })
            </script>

            <style>
                .dashboard .tiles:hover{
                    cursor: pointer;
                }
            </style>

            <div class="row">
                <div class="col-md-4 col-vlg-3 col-sm-6">
                    <div class="tiles green m-b-10">
                        <div class="tiles-body">
                            <div class="controller">

                            </div>
                            <div class="tiles-title text-black">CASES </div>
                            <div class="widget-stats">
                                <div class="wrapper transparent">
                                    <span class="item-title">Available</span>
                                    <span class="item-count animate-number semi-bold" data-value="{{count($availableCases)}}" data-animation-duration="700">0</span>
                                </div>
                            </div>
                            <div class="widget-stats">
                                <div class="wrapper transparent">
                                    <span class="item-title">Pending</span>
                                    <span class="item-count animate-number semi-bold" data-value="{{count($pendingCases)}}" data-animation-duration="700">0</span>
                                </div>
                            </div>
                            <div class="widget-stats ">
                                <div class="wrapper last">
                                    <span class="item-title">Completed</span>
                                    <span class="item-count animate-number semi-bold" data-value="{{count($completedCases)}}" data-animation-duration="700">0</span>
                                </div>
                            </div>
                            <div class="progress transparent progress-small no-radius m-t-20" style="width:90%">
                                <div class="progress-bar progress-bar-white animate-progress-bar" data-percentage="64.8%"></div>
                            </div>
                            <div class="description"> <span class="text-white mini-description ">4% higher <span class="blend">than last month</span></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-vlg-3 col-sm-6">
                    <div class="tiles blue m-b-10">
                        <div class="tiles-body">
                            <div class="controller">
                                {{--<a href="javascript:;" class="reload"></a>--}}
                                {{--<a href="javascript:;" class="remove"></a>--}}
                            </div>
                            <div class="tiles-title text-black">Patients </div>
                            <div class="widget-stats">
                                <div class="wrapper transparent">
                                    <span class="item-title">Today</span> <span class="item-count animate-number semi-bold" data-value="{{count($patientsToday)}}" data- animation-duration="700">0</span>
                                </div>
                            </div>
                            <div class="widget-stats">
                                <div class="wrapper transparent">
                                    <span class="item-title">This Month</span> <span class="item-count animate-number semi-bold" data-value="{{count($patientsThisMonth)}}" data-animation-duration="700">0</span>
                                </div>
                            </div>
                            <div class="widget-stats ">
                                <div class="wrapper last">
                                    <span class="item-title">Total</span> <span class="item-count animate-number semi-bold" data-value="{{count($patients)}}" data-animation-duration="700">0</span>
                                </div>
                            </div>
                            <div class="progress transparent progress-small no-radius m-t-20" style="width:90%">
                                <div class="progress-bar progress-bar-white animate-progress-bar" data-percentage="54%"></div>
                            </div>
                            <div class="description"> <span class="text-white mini-description ">4% higher <span class="blend">than last month</span></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-vlg-3 col-sm-6">
                    <div class="tiles red m-b-10">
                        <div class="tiles-body">
                            <div class="controller">
                                {{--<a href="javascript:;" class="reload"></a>--}}
                                {{--<a href="javascript:;" class="remove"></a>--}}
                            </div>
                            <div class="tiles-title text-black">DOCTORS </div>
                            <div class="widget-stats">
                                <div class="wrapper transparent">
                                    <span class="item-title">Today</span> <span class="item-count animate-number semi-bold" data-value="{{count($doctorsToday)}}" data-animation-duration="700">0</span>
                                </div>
                            </div>
                            <div class="widget-stats">
                                <div class="wrapper transparent">
                                    <span class="item-title">This Month</span> <span class="item-count animate-number semi-bold" data-value="{{count($doctorsThisMonth)}}" data-animation-duration="700">0</span>
                                </div>
                            </div>
                            <div class="widget-stats ">
                                <div class="wrapper last">
                                    <span class="item-title">Total</span> <span class="item-count animate-number semi-bold" data-value="{{count($doctors)}}" data-animation-duration="700">0</span>
                                </div>
                            </div>
                            <div class="progress transparent progress-small no-radius m-t-20" style="width:90%">
                            <div class="progress-bar progress-bar-white animate-progress-bar" data-percentage="64.8%"></div>
                            </div>
                            <div class="description"> <span class="text-white mini-description ">4% higher <span class="blend">than last month</span></span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-3 col-vlg-3 col-sm-6">
                    <div class="tiles purple m-b-10">
                        <div class="tiles-body">
                            <div class="controller">
                            </div>
                            <div class="tiles-title text-black">PHARMACIES </div>
                            <div class="widget-stats">
                                <div class="wrapper transparent">
                                    <span class="item-title">Today</span> <span class="item-count animate-number semi-bold" data-value="{{count($pharmaciesToday)}}" data-animation-duration="700">0</span>
                                </div>
                            </div>
                            <div class="widget-stats">
                                <div class="wrapper transparent">
                                    <span class="item-title">This Month</span> <span class="item-count animate-number semi-bold" data-value="{{count($pharmaciesThisMonth)}}" data-animation-duration="700">0</span>
                                </div>
                            </div>
                            <div class="widget-stats ">
                                <div class="wrapper last">
                                    <span class="item-title">Total</span> <span class="item-count animate-number semi-bold" data-value="{{count($pharmacies)}}" data-animation-duration="700">0</span>
                                </div>
                            </div>
                            <div class="progress transparent progress-small no-radius m-t-20" style="width:90%">
                            <div class="progress-bar progress-bar-white animate-progress-bar" data-percentage="90%"></div>
                            </div>
                            <div class="description"> <span class="text-white mini-description ">4% higher <span class="blend">than last month</span></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-vlg-3 col-sm-6">
                    <div class="tiles green m-b-10">
                        <div class="tiles-body">
                            <div class="controller">
                                {{--<a href="javascript:;" class="reload"></a>--}}
                                {{--<a href="javascript:;" class="remove"></a>--}}
                            </div>
                            <div class="tiles-title text-black">HOSPITALS </div>
                            <div class="widget-stats">
                                <div class="wrapper transparent">
                                    <span class="item-title">Today</span>
                                    <span class="item-count animate-number semi-bold" data-value="{{count($hospitalsToday)}}" data-animation-duration="700">0</span>
                                </div>
                            </div>
                            <div class="widget-stats">
                                <div class="wrapper transparent">
                                    <span class="item-title">This Month</span>
                                    <span class="item-count animate-number semi-bold" data-value="{{count($hospitalsThisMonth)}}" data-animation-duration="700">0</span>
                                </div>
                            </div>
                            <div class="widget-stats ">
                                <div class="wrapper last">
                                    <span class="item-title">Total</span>
                                    <span class="item-count animate-number semi-bold" data-value="{{count($hospitals)}}" data-animation-duration="700">0</span>
                                </div>
                            </div>
                            <div class="progress transparent progress-small no-radius m-t-20" style="width:90%">
                                <div class="progress-bar progress-bar-white animate-progress-bar" data-percentage="64.8%"></div>
                            </div>
                            <div class="description"> <span class="text-white mini-description ">{{$hospitalGrowth}}% higher <span class="blend">than last month</span></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-vlg-3 col-sm-6">
                    <div class="tiles blue m-b-10">
                        <div class="tiles-body">
                            <div class="controller">
                                {{--<a href="javascript:;" class="reload"></a>--}}
                                {{--<a href="javascript:;" class="remove"></a>--}}
                            </div>
                            <div class="tiles-title text-black">Labs </div>
                            <div class="widget-stats">
                                <div class="wrapper transparent">
                                    <span class="item-title">Today</span> <span class="item-count animate-number semi-bold" data-value="{{count($labsToday)}}" data- animation-duration="700">0</span>
                                </div>
                            </div>
                            <div class="widget-stats">
                                <div class="wrapper transparent">
                                    <span class="item-title">This Month</span> <span class="item-count animate-number semi-bold" data-value="{{count($labsThisMonth)}}" data-animation-duration="700">0</span>
                                </div>
                            </div>
                            <div class="widget-stats ">
                                <div class="wrapper last">
                                    <span class="item-title">Total</span> <span class="item-count animate-number semi-bold" data-value="{{count($labs)}}" data-animation-duration="700">0</span>
                                </div>
                            </div>
                            <div class="progress transparent progress-small no-radius m-t-20" style="width:90%">
                                <div class="progress-bar progress-bar-white animate-progress-bar" data-percentage="54%"></div>
                            </div>
                            <div class="description"> <span class="text-white mini-description ">4% higher <span class="blend">than last month</span></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-vlg-3 col-sm-6">
                    <div class="tiles red m-b-10">
                        <div class="tiles-body">
                            <div class="controller">

                            </div>
                            <div class="tiles-title text-black">PAYMENTS </div>
                            <div class="widget-stats">
                                <div class="wrapper transparent">
                                    <span class="item-title">Today</span> <span class="item-count animate-number semi-bold" data-value="{{count($paymentsToday)}}" data-animation-duration="700">0</span>
                                </div>
                            </div>
                            <div class="widget-stats">
                                <div class="wrapper transparent">
                                    <span class="item-title">This Month</span> <span class="item-count animate-number semi-bold" data-value="{{count($paymentsThisMonth)}}" data-animation-duration="700">0</span>
                                </div>
                            </div>
                            <div class="widget-stats ">
                                <div class="wrapper last">
                                    <span class="item-title">Total</span> <span class="item-count animate-number semi-bold" data-value="{{count($payments)}}" data-animation-duration="700">0</span>
                                </div>
                            </div>
                            <div class="progress transparent progress-small no-radius m-t-20" style="width:90%">
                            <div class="progress-bar progress-bar-white animate-progress-bar" data-percentage="64.8%"></div>
                            </div>
                            <div class="description"> <span class="text-white mini-description ">4% higher <span class="blend">than last month</span></span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">
                <div>

                    @foreach($loungeItems as $item)
                        <div class="col-md-4 m-b-10">
                            <div class="widget-item ">
                                <div class="controller overlay right">


                                </div>
                                <div class="tiles green  overflow-hidden full-height" style="max-height:214px">
                                    <div class="overlayer bottom-right fullwidth">
                                        <div class="overlayer-wrapper">
                                            <div class="tiles gradient-black p-l-20 p-r-20 p-b-20 p-t-20">
                                                <div class="pull-right"> <a href="{{url('lounge/' . $item->lid)}}" class="hashtags transparent"> {{$item->title}} </a> </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <img src="{{$item->image}}" style="width:100%" alt="" class="lazy hover-effect-img"> </div>
                                <div class="tiles white ">
                                    <div class="tiles-body">
                                        <div class="row">


                                            <div class="user-profile-pic text-left">
                                                <div class="pull-right m-r-20 m-t-35">
                                                    <span class="text-black small-text">{{Carbon::createFromFormat("Y-m-d H:i:s",$item->created_at)->diffForHumans()}}</span>
                                                    <br>
                                                    <span>{{$item->views}} views</span>
                                                </div>
                                            </div>
                                            <div class="col-md-5 no-padding">
                                                <div class="user-comment-wrapper">
                                                    <div class="comment">
                                                        <div class="user-name text-black">
                                                            @if(isset($item->company))
                                                                {{$item->company}}
                                                            @else
                                                                Doctap
                                                            @endif
                                                        </div>
                                                    </div>
                                                    {{--<div class="clearfix"></div>--}}
                                                </div>
                                            </div>

                                            {{--<div class="col-md-7 no-padding">--}}
                                            {{--<div class="clearfix"></div>--}}
                                            {{--<div class="m-r-20 m-t-20 m-b-10  m-l-10">--}}
                                            {{--<p class="p-b-10">The attention to detail and the end product is stellar! I enjoyed the process </p>--}}
                                            {{--<a href="#" class="hashtags m-b-5"> #new york city </a> <a href="#" class="hashtags m-b-5"> #amazing </a> <a href="#" class="hashtags m-b-5"> #citymax </a> </div>--}}
                                            {{--</div>--}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach


                </div>
                <div class="col-md-4 m-b-10 ">
                    <div class="col-md-12">
                        <div class="row tiles-container">
                            <div class="col-md-6 col-sm-6 tiles dark-blue" style="height:141px">
                                <div class="overlayer top-right">
                                    <div class="p-r-15 p-t-15"> <i class="fa fa-facebook text-white fa-4x"></i> </div>
                                </div>
                                <div class="overlayer bottom-left">
                                    <div class="p-l-15 p-b-15">
                                        <h2 class="text-white">5m</h2>
                                        <h4 class="text-white">Likes</h4>
                                        {{--<span class="text-white mini-description ">2% higher than last month</span> --}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 tiles light-blue" style="height:141px">
                                <div class="overlayer top-right">
                                    <div class="p-r-15 p-t-15"> <i class="fa fa-twitter text-white fa-4x"></i> </div>
                                </div>
                                <div class="overlayer bottom-left">
                                    <div class="p-l-15 p-b-15">
                                        <h2 class="text-white">14k</h2>
                                        <h4 class="text-white">tweets</h4>
                                        {{--<span class="text-white mini-description ">2% higher than last month</span> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row tiles-container">
                            <div class="col-md-6 col-sm-6 tiles red" style="height:141px">
                                <div class="overlayer top-right">
                                    <div class="p-r-15 p-t-15"> <i class="fa fa-instagram text-white fa-4x"></i> </div>
                                </div>
                                <div class="overlayer bottom-left">
                                    <div class="p-l-15 p-b-15">
                                        <h2 class="text-white">154</h2>
                                        <h4 class="text-white">circles</h4>
                                        {{--<span class="text-white mini-description ">2% higher than last month</span> --}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 tiles light-red" style="height:141px">
                                <div class="overlayer top-right">
                                    <div class="p-r-15 p-t-15"> <i class="fa fa-linkedin text-white fa-4x"></i> </div>
                                </div>
                                <div class="overlayer bottom-left">
                                    <div class="p-l-15 p-b-15">
                                        <h2 class="text-white">1550</h2>
                                        <h4 class="text-white">Subscribers</h4>
                                        {{--<span class="text-white mini-description ">2% higher than last month</span> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 ">
                    <div class="tiles white m-b-10 clearfix">
                        <div class="col-md-4 col-sm-12 no-padding">
                            <div id="world2" style="height:405px"></div>
                        </div>
                        <div class="col-md-8 p-t-35 p-r-20 p-b-30 col-sm-12">
                            <div class="col-md-6 col-sm-6 ">
                                <div class="row b-b b-grey p-b-10">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <p class="bold small-text">USERS</p>
                                        <h3 class="bold text-success">2,455,559</h3>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <p class="bold small-text">NEW USERS</p>
                                        <h3 class="bold text-black">548</h3>
                                    </div>
                                </div>
                                <div class="row m-t-15">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <p class="bold text-black">Finland</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <p class="text-black">245 <i class="fa fa-sort-asc fa-lg text-error " style="vertical-align: super;"></i></p>
                                    </div>
                                </div>
                                <div class="row m-t-10">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <p class="bold text-black">France</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <p class="text-black">599 <i class="fa fa-sort-desc fa-lg text-success "></i></p>
                                    </div>
                                </div>
                                <div class="row m-t-10">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <p class="bold text-black">United Kingdom</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <p class="text-black">800 <i class="fa fa-sort-desc fa-lg text-success "></i></p>
                                    </div>
                                </div>
                                <div class="row m-t-10">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <p class="bold text-black">Italy</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <p class="text-black">450 <i class="fa fa-sort-asc fa-lg text-black " style="vertical-align: super;"></i></p>
                                    </div>
                                </div>
                                <div class="row m-t-10 xs-m-b-20">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <p class="bold text-black">Canada</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <p class="text-error">155 <i class="fa fa-sort-asc fa-lg text-error " style="vertical-align: super;"></i></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="row b-b b-grey p-b-10">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <p class="bold small-text">SESSIONS</p>
                                        <h3 class="bold text-success">549</h3>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <p class="bold small-text">AVG.SESSIONS</p>
                                        <h3 class="bold text-black">15.58%</h3>
                                    </div>
                                </div>
                                <div class="row m-t-15">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <p class="bold text-black">Finland</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <p class="text-black">245 <i class="fa fa-sort-asc fa-lg text-error " style="vertical-align: super;"></i></p>
                                    </div>
                                </div>
                                <div class="row m-t-10">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <p class="bold text-black">France</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <p class="text-black">599 <i class="fa fa-sort-desc fa-lg text-success "></i></p>
                                    </div>
                                </div>
                                <div class="row m-t-10">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <p class="bold text-black">United Kingdom</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <p class="text-black">800 <i class="fa fa-sort-desc fa-lg text-success "></i></p>
                                    </div>
                                </div>
                                <div class="row m-t-10">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <p class="bold text-black">Italy</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <p class="text-black">450 <i class="fa fa-sort-asc fa-lg text-black " style="vertical-align: super;"></i></p>
                                    </div>
                                </div>
                                <div class="row m-t-10">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <p class="bold text-black">Canada</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <p class="text-error">155 <i class="fa fa-sort-asc fa-lg text-error " style="vertical-align: super;"></i></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>

            {{--<div class="row 2col dashboard">--}}
                {{--<div class="col-md-4 col-sm-6 m-b-10">--}}
                    {{--<div class="tiles blue casesDiv">--}}
                        {{--<div class="tiles-body">--}}
                            {{--<div class="controller">--}}
                                {{--<a href="javascript:;" class="reload"></a>--}}
                            {{--</div>--}}
                            {{--<div class="tiles-title"> Cases </div>--}}
                            {{--<div class="heading"> Available </div>--}}
                            {{--<div class="heading"> <span id="availableCases" class="animate-number" data-value="{{count($availableCases)}}" data-animation-duration="1200">0</span> </div>--}}
                            {{--<div class="progress transparent progress-small no-radius">--}}
                                {{--<div class="progress-bar progress-bar-white animate-progress-bar" data-percentage="26.8%"></div>--}}
                            {{--</div>--}}
                            {{--<div class="description"><i class="icon-custom-right"></i><span class="text-white mini-description ">&nbsp; {{count($availableCases)}} <span class="blend">today</span></span>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}

                {{--<div class="col-md-4 col-sm-6 m-b-10">--}}
                    {{--<div class="tiles casesDiv" style="background-color: darkorange">--}}
                        {{--<div class="tiles-body">--}}
                            {{--<div class="controller">--}}
                                {{--<a href="javascript:;" class="reload"></a>--}}
                            {{--</div>--}}
                            {{--<div class="tiles-title"> Cases </div>--}}
                            {{--<div class="heading"> Pending </div>--}}
                            {{--<div class="heading"> <span id="pendingCases" class="animate-number" data-value="{{count($pendingCases)}}" data-animation-duration="1200">0</span> </div>--}}
                            {{--<div class="description"><i class="icon-custom-right"></i><span class="text-white mini-description ">&nbsp; {{count($pendingCases)}} <span class="blend">today</span></span>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}

                {{--<div class="col-md-4 col-sm-6 m-b-10">--}}
                    {{--<div class="tiles casesDiv" style="background-color: green">--}}
                        {{--<div class="tiles-body">--}}
                            {{--<div class="controller">--}}
                                {{--<a href="javascript:;" class="reload"></a>--}}
                            {{--</div>--}}
                            {{--<div class="tiles-title"> Cases </div>--}}
                            {{--<div class="heading"> Complete </div>--}}
                            {{--<div class="heading"> <span id="completedCases" class="animate-number" data-value="{{count($completedCases)}}" data-animation-duration="1200">0</span> </div>--}}
                            {{--<div class="description"><i class="icon-custom-right"></i><span class="text-white mini-description ">&nbsp; {{count($pendingCases)}} <span class="blend">today</span></span>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}


                {{--<!-- end cases  -->--}}

                {{--<div class="col-md-6 col-sm-6 m-b-10">--}}
                    {{--<div id="patientsDiv" class="tiles green ">--}}
                        {{--<div class="tiles-body">--}}
                            {{--<div class="controller">--}}
                                {{--<a href="javascript:;" class="reload"></a>--}}
                            {{--</div>--}}
                            {{--<div class="heading"> Patients </div>--}}
                            {{--<div class="heading"> <span id="patients" class="animate-number" data-value="{{count($patients)}}" data-animation-duration="1000">0</span> </div>--}}
                            {{--<div class="description"><i class="icon-custom-right"></i><span id="patientsToday" class="text-white mini-description ">&nbsp; {{count($patientsToday)}} <span class="blend">signed up today</span></span>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-6 col-sm-6 m-b-10">--}}
                    {{--<div id="doctorsDiv" class="tiles red ">--}}
                        {{--<div class="tiles-body">--}}
                            {{--<div class="controller">--}}
                                {{--<a href="javascript:;" class="reload"></a>--}}
                            {{--</div>--}}
                            {{--<div class="heading"> Doctors</div>--}}
                            {{--<div class="heading"> <span id="doctors" class="animate-number" data-value="{{count($doctors)}}" data-animation-duration="1200">0</span> </div>--}}
                            {{--<div class="description"><i class="icon-custom-right"></i><span id="doctorsToday" class="text-white mini-description ">&nbsp;{{count($doctorsToday)}} <span class="blend">signed up today</span></span>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}

            {{--</div>--}}

            {{--<div class="row 2col">--}}
                {{--<div class="col-md-3 col-sm-6 m-b-10">--}}
                    {{--<div id="hospitalsDiv" class="tiles purple  ">--}}
                        {{--<div class="tiles-body">--}}
                            {{--<div class="controller">--}}
                                {{--<a href="javascript:;" class="reload"></a>--}}
                            {{--</div>--}}
                            {{--<div class="heading"> Hospitals </div>--}}
                            {{--<div class="row-fluid">--}}
                                {{--<div class="heading"> <span id="hospitals" class="animate-number" data-value="{{count($hospitals)}}" data-animation-duration="700">0</span> </div>--}}
                            {{--</div>--}}
                            {{--<div class="description"><i class="icon-custom-right"></i><span id="hospitalsToday" class="text-white mini-description ">&nbsp;{{count($hospitalsToday)}} <span class="blend">were added today</span></span>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-3 col-sm-6 m-b-10">--}}
                    {{--<div id="labsDiv" class="tiles red ">--}}
                        {{--<div class="tiles-body">--}}
                            {{--<div class="controller">--}}
                                {{--<a href="javascript:;" class="reload"></a>--}}
                            {{--</div>--}}
                            {{--<div class="heading"> Labs </div>--}}
                            {{--<div class="heading"> <span id="labs" class="animate-number" data-value="{{count($labs)}}" data-animation-duration="1200">0</span> </div>--}}
                            {{--<div class="description"><i class="icon-custom-right"></i><span id="labsToday" class="text-white mini-description ">&nbsp; {{count($labsToday)}} <span class="blend">were added today</span></span>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-3 col-sm-6 m-b-10">--}}
                    {{--<div id="pharmaciesDiv" class="tiles green ">--}}
                        {{--<div class="tiles-body">--}}
                            {{--<div class="controller">--}}
                                {{--<a href="javascript:;" class="reload"></a>--}}
                            {{--</div>--}}
                            {{--<div class="heading"> Pharmacies </div>--}}
                            {{--<div class="heading"> <span id="pharmacies" class="animate-number" data-value="{{count($pharmacies)}}" data-animation-duration="1000">0</span> </div>--}}
                            {{--<div class="description"><i class="icon-custom-right"></i><span id="pharmaciesToday" class="text-white mini-description ">&nbsp; {{count($pharmaciesToday)}} <span class="blend">were added today</span></span>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}

                {{--<div class="col-md-3 col-sm-6 m-b-10">--}}
                    {{--<div id="revenueDiv" class="tiles blue  ">--}}
                        {{--<div class="tiles-body">--}}
                            {{--<div class="controller">--}}
                                {{--<a href="javascript:;" class="reload"></a>--}}
                            {{--</div>--}}
                            {{--<div class="heading"> Revenue </div>--}}
                            {{--<div class="row-fluid">--}}
                                {{--<div class="heading"> $ <span id="revenue" class="animate-number" data-value="{{count($payments)}}" data-animation-duration="700">0</span> </div>--}}
                            {{--</div>--}}
                            {{--<div class="description"><i class="icon-custom-right"></i><span id="revenueToday" class="text-white mini-description ">&nbsp; ${{count($paymentsToday)}} <span class="blend">generated today</span></span>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}

        </div>
    </div>

    <script>
        $(document).ready(function(){

            var previousComplete;
            var previousPending;
            var previousAvailable;


            setInterval(function () {
                $.ajax({
                    url:"{{url('api/live-update')}}",
                    method: "post",
                    data:{
                        _token : "{{csrf_token()}}"
                    },
                    success: function (response) {

                        if(
                        response.availableCases > previousAvailable
                        ){
                        var audioElement = document.createElement('audio');
                        audioElement.setAttribute('src', "{{url('notification.mp3')}}");
                        audioElement.play();
                        //
                        }

                        $('#availableCases').text(response.availableCases);
                        $('#pendingCases').text(response.pendingCases);
                        $('#completedCases').text(response.completedCases);

                        $('#patients').text(response.patients );

                        $('#patientsToday').html(" " + response.patientsToday + " <span class=\"blend\">signed up today</span>");
                        $('#doctors').text(response.doctors);
                        $('#doctorsToday').html(" " + response.doctorsToday + " <span class=\"blend\">signed up today</span>");

                        $('#hospitals').text(response.hospitals);
                        $('#hospitalsToday').text(response.hospitalsToday);

                        $('#labs').text(response.labs);
                        $('#labsToday').text(response.labsToday);

                        $('#pharmacies').text(response.pharmacies);
                        $('#pharmaciesToday').text(response.pharmaciesToday);


                        previousAvailable  = response.availableCases;
                        previousPending   = response.pendingCases;
                        previousComplete = response.completedCases;

                    },
                    error: function (error) {

                    }
                });

            },5000);
        });
    </script>


@endsection












