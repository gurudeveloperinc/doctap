@extends('layouts.auth')

@section('content')
    <style>
        div.form-group input, div.form-group select {
            margin: 10px 0 !important;
        }
    </style>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @include('notification')
                <div class=" box-typical box-typical-dashboard box-typical-padding panel panel-default">
                    <div class="panel-heading">Register</div>
                    <div class="panel-body">
                        <form class="form-horizontal" enctype="multipart/form-data" method="POST" action="{{ route('register') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('fname') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Firstname</label>

                                <div class="col-md-6">
                                    <input id="fname" type="text" class="form-control" name="fname" value="{{ old('fname') }}" required autofocus>

                                    @if ($errors->has('fname'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('fname') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('sname') ? ' has-error' : '' }}">
                                <label for="sname" class="col-md-4 control-label">Surname</label>

                                <div class="col-md-6">
                                    <input id="sname" type="text" class="form-control" name="sname" value="{{ old('sname') }}" required autofocus>

                                    @if ($errors->has('sname'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('sname') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="phone" class="col-md-4 control-label">Phone</label>

                                <div class="col-md-6">
                                    <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" >
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="" class="col-md-4 control-label">Address</label>

                                <div class="col-md-6">
                                    <textarea id="address" class="form-control" name="address"></textarea>

                                </div>
                            </div>


                            <div class="form-group">
                                <label for="gender" class="col-md-4 control-label">Role</label>

                                <div class="col-md-6">
                                    <select id="role" onchange="roleChanged()" name="role" class="form-control">
                                        <option>Pharmacy</option>
                                        <option>Lab</option>
                                        <option>Hospital</option>
                                        <option>Staff</option>
                                        <option>Admin</option>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="image" class="col-md-4 control-label">Image</label>

                                <div class="col-md-6">
                                    <input id="image" type="file" class="form-control" name="image">
                                </div>
                            </div>



                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-4 control-label">Password</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control" name="password" minlength="6" required>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" minlength="6" class="form-control" name="password_confirmation" required>
                                </div>
                            </div>

                            <div id="place" >

                                <br><br>
                                <hr>
                                <div class="form-group">
                                    <label for="name" class="col-md-12 control-label">
                                        <b>Institution Details</b>
                                    </label>
                                </div>


                                 <div class="form-group">
                                    <label for="name" class="col-md-4 control-label">Name</label>

                                    <div class="col-md-6">
                                        <input id="placename" type="text" class="form-control" name="placename" value="{{ old('placename') }}" autofocus>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="" class="col-md-4 control-label">Address</label>

                                    <div class="col-md-6">
                                        <textarea id="placeaddress" class="form-control" name="placeaddress"></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="gender" class="col-md-4 control-label">City</label>

                                    <div class="col-md-6">
                                        <select id="placecity" name="placecity" class="form-control">
                                            <option>Abuja</option>
                                            <option>Lagos</option>
                                            <option>Port-Harcourt</option>
                                            <option>Ebonyi</option>
                                        </select>
                                    </div>
                                </div>


                            </div>
                            <div class="form-group ">
                                <div class="col-md-6 col-md-offset-6">
                                    <button type="submit" class="btn btn-primary">
                                        Register
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <script>

        function roleChanged(){
            var place = $('#place');
            var role = $('#role');

            if(role.val() == 'Admin' || role.val() == 'Staff'){
                place.addClass('hidden');
            }

            if(role.val() == 'Pharmacy'){
                place.removeClass('hidden');
            }
            if(role.val() == 'Lab'){
                place.removeClass('hidden');
            }
            if(role.val() == 'Hospital'){
                place.removeClass('hidden');
            }

        }

        $(document).ready(function () {


        });
    </script>
@endsection
