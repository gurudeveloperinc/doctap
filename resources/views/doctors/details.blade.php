<?php use Carbon\Carbon; ?>
@extends('layouts.admin')

@section('content')

    <div class="page-content">

        <div class="clearfix"></div>
        <div class="content sm-gutter">
            @include('notification')
            <div class="page-title">
                <a href="{{url('manage-doctors')}}">
                    <i class="icon-custom-left"></i>
                </a>

                <div class="pull-right">
                    @if($doctor->isConfirmed == 0)
                        <a href="{{url('confirm-doctor/' . $doctor->docid)}}" class="btn btn-primary">Confirm Doctor</a>
                    @endif

                    @if($doctor->isSuspended == 0)
                            <a href="{{url('suspend-doctor/' . $doctor->docid)}}" class="btn btn-danger">Suspend Doctor</a>
                    @else
                            <a href="{{url('unsuspend-doctor/' . $doctor->docid)}}" class="btn btn-warning">Readmit Doctor</a>
                    @endif
                </div>
            </div>
        </div>


        <div class="row-fluid">
            <div class="grid simple col-md-10 col-md-offset-1">
                <div class="grid-title no-border">
                </div>
                <div class="grid-body no-border">
                    <br>
                    <div class="row">
                        <div class="col-md-3">
                            <img src="{{$doctor->image}}" alt="doctors profile image" style="width: 100%" class="img img-thumbnail"/>
                            @if($doctor->isConfirmed == 1)
                                <label class="label label-success">Confirmed</label>
                            @else
                                <label class="label label-danger">Un-Confirmed</label>
                            @endif

                            @if($doctor->isSuspended == 1)
                                <label class="label label-danger">Suspended</label>
                            @endif

                            <h4 align="center" style="margin-top: 20px">
                                @for( $i = 0; $i < number_format($doctor->rating,0); $i++)
                                <i class="fa fa-star" style="color: gold;"></i>
                                @endfor
                                <br>
                                <span style="font-size: 12px;">
                                    <b>
                                    {{number_format($doctor->rating,1)}} of 5 ( {{$feedbackCount}} review
                                        @if($feedbackCount > 1)
                                            s
                                        @endif
                                        )
                                    </b>

                                </span>

                            </h4>



                        </div>


                        <div class="col-md-8">
                            <h3 style="margin: 0 0 10px 0;"><span class="semi-bold">{{$doctor->fname}} {{$doctor->sname}}</span></h3>
                        </div>

                        <div class="form-group col-md-3 col-sm-12">
                            <label class="form-label">License ID</label><br>
                            <span class="help">{{$doctor->licenseid}}</span>
                            <br>
                            <label class="form-label">Email</label><br>
                            <span class="help">{{$doctor->email}}</span>
                            <br>
                            <label class="form-label">Phone</label><br>
                            <span class="help">{{$doctor->phone}}</span>
                            <br>
                            <label class="form-label">Nationality</label><br>
                            <span class="help">{{$doctor->nationality}}</span>
                            <br>
                            <label class="form-label">Total Cases</label><br>
                            <span class="help">{{count($doctor->Cases)}}</span>

                        </div>

                        <div class="form-group col-md-3 col-sm-12">

                            <label class="form-label">Country of practice</label><br>
                            <span class="help">{{$doctor->countryOfPractice}}</span>
                            <br>
                            <label class="form-label">Years Of Experience</label><br>
                            <span class="help">{{$doctor->yearsOfExperience}}</span>
                            <br>
                            <label class="form-label">Hospital Affiliation</label> <br>
                            <span class="help">{{$doctor->hospitalAffiliation}}</span>
                            <br>
                            <label class="form-label">Gender</label><br>
                            <span class="help">{{$doctor->gender}}</span>
                        </div>
                        <div class="row form-group col-md-3">

                            {{--<h4>Payment Details</h4>--}}
                            {{--<br>--}}
                            <label class="form-label">Payment Method</label><br>
                            <span class="help">{{$doctor->paymentMethod}}</span>

                            @if($doctor->paymentMethod == 'paypal')
                                <br>
                                <label class="form-label">Paypal Email</label><br>
                                <span class="help">{{$doctor->paypal}}</span>
                            @else

                                <br>
                                <label class="form-label">Account Name</label><br>
                                <span class="help">{{$doctor->accountName}}</span>

                                <br>
                                <label class="form-label">Account Number</label><br>
                                <span class="help">{{$doctor->accountNumber}}</span>
                                <br>
                                <label class="form-label">Account Bank</label><br>
                                <span class="help">{{$doctor->accountBank}}</span>

                            @endif
                        </div>

                        <div class="col-md-12">
                            <div class=" col-md-3 col-md-offset-3">
                                <label class="form-label">Available Earnings</label>
                                <span class="help" style="font-size:16px;color:#098A54"><b>₦{{number_format($doctor->earnings,0)}}</b></span>
                            </div>
                            <div class=" col-md-3 ">
                                <label class="form-label">Pending Earnings</label>
                                <span class="help" style="font-size:16px;color:#098A54"><b>₦{{number_format($totalPendingMaturity,0)}}</b></span>
                            </div>

                            <div class=" col-md-3">
                                <label class="form-label">Total Earnings</label>
                                <span class="help" style="font-size:16px;color:#098A54"><b>₦{{number_format($doctor->totalEarnings,0)}}</b></span>

                            </div>

                        </div>

                    </div>
                </div>
            </div>

        </div>



        <div class="row-fluid">
            <div class="grid simple col-md-10 col-md-offset-1">
                <div>

                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">
                                Specializations
                            </a></li>
                        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Uploaded Files</a></li>
                        <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Cases</a></li>
                        <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Payments</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="home">
                            <div class="grid-body no-border">

                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Specialization</th>
                                        <th>Description</th>
                                        <th>Date Added</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($doctor->Specialization as $specialization)
                                        <tr>
                                            <td>{{$specialization->Specialization->name}}</td>
                                            <td>{{$specialization->Specialization->description}}</td>
                                            <td>{{$specialization->created_at->toDayDateTimeString()}}</td>
                                            <td>
                                                <a href="{{url('remove-doctor-specialization/' . $specialization->dsid)}}">remove</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>


                            </div>


                        </div>
                        <div role="tabpanel" class="tab-pane" id="profile">
                            <div class="grid-body no-border">

                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Doctor</th>
                                        <th>Date Started</th>
                                        <th>Date Ended</th>
                                        <th>Notes</th>
                                    </tr>
                                    </thead>

                                        <tbody>

                                        @foreach($doctor->Files as $file)
                                            <tr>
                                                <td>{{$file->title}}</td>
                                                <td>{{$file->description}}</td>
                                                <td>{{$file->created_at->toDayDateTimeString()}}</td>
                                                <td>
                                                    <a target="_blank" href="{{$file->url}}">View</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>

                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane" id="messages">
                            <div class="grid-body no-border">

                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Case ID</th>
                                        <th>Status</th>
                                        <th>Patient</th>
                                        <th>Date Started</th>
                                        <th>Date Ended</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <?php $doctorCases = $doctor->Cases()->paginate(10); ?>

                                    @foreach($doctorCases as $case)
                                        <tr>
                                            <td>{{$case->casid}}</td>
                                            <td>
                                                @if($case->status == 'Available')
                                                    <label class="label label-primary">Available</label>
                                                @endif
                                                @if($case->status == 'Pending')
                                                    <label class="label label-warning">Pending</label>
                                                @endif
                                                @if($case->status == 'Completed')
                                                    <label class="label label-success">Completed</label>
                                                @endif
                                                @if($case->status == 'Cancelled')
                                                    <label class="label label-danger">Cancelled</label>
                                                @endif

                                            </td>
                                            <td>
                                                @if($case->type == 'patient')
                                                    <a href="{{url('patient/' . $case->Patient->patid)}}">
                                                        {{$case->Patient->fname}} {{$case->Patient->sname}}
                                                    </a>
                                                @endif

                                                @if($case->type == 'dependant')
                                                    <a href="{{url('dependant/' . $case->Dependant->pdid)}}">
                                                        {{$case->Dependant->fname}} {{$case->Dependant->sname}}
                                                    </a>
                                                @endif

                                            </td>
                                            <td>{{$case->created_at->toDayDateTimeString()}}</td>
                                            <td>
                                                @if(isset($case->ended_at))
                                                    {{Carbon::createFromFormat("Y-m-d H:i:s",$case->ended_at)->toDayDateTimeString()}}
                                                @endif
                                            </td>

                                            <td>
                                                <a href="{{url('case/' . $case->casid)}}">View</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{$doctorCases->render()}}
                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane" id="settings">

                            <div class="grid-body no-border">

                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Amount (&#x20A6;)</th>
                                        <th>Date Paid</th>
                                    </tr>
                                    </thead>

                                    <tbody>

                                    @foreach($doctor->Payments as $payment)
                                        <tr>
                                            <td>{{$payment->dcpid}}</td>
                                            <td>{{$payment->amount}}</td>
                                            <td>{{$payment->created_at->toDayDateTimeString()}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
























    </div>
@endsection