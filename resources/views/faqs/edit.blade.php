@extends('layouts.admin')

@section('content')

    <div class="page-content">

        <div class="content sm-gutter">
            @include('notification')
            <div class="page-title">
                <a href="{{url('manage-faqs')}}">
                    <i class="icon-custom-left"></i>
                </a>
                <div class="clearfix"></div>
            </div>


            <div class="row-fluid">

                <div class="span12">
                    <div class="grid simple ">
                        <div class="grid-title">
                            <h4>Edit <span class="semi-bold">FAQ</span></h4>
                            <div class="tools">
                                <a href="javascript:;" class="expand"></a>
                                <a href="#grid-config" data-toggle="modal" class="config"></a>
                                <a href="javascript:;" class="reload"></a>
                                <a href="javascript:;" class="remove"></a>
                            </div>
                        </div>
                        <div class="grid-body ">

                            <form method="post" action="{{url('edit-faq/' . $faq->fid)}}">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label>Question</label>
                                    <input type="text" class="form-control" value="{{$faq->question}}" name="question">
                                </div>

                                <div class="form-group">
                                    <label>Answer</label>
                                    <textarea name="answer" class="form-control">{{$faq->answer}}</textarea>
                                </div>

                                <button class="btn btn-primary">Save</button>

                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection