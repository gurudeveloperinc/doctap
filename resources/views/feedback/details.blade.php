@extends('layouts.admin')

@section('content')

    <div class="page-content">

        <div class="clearfix"></div>
        <div class="content sm-gutter">
            @include('notification')
            <div class="page-title">
                <a href="{{url('feedback')}}">
                    <i class="icon-custom-left"></i>
                </a>
                <h3>Feedback <span>#{{$feedback->fdid}}</span></h3>

                <div class="pull-right">
                    @if($feedback->isViewed == 0)
                        <a href="{{url('feedback-viewed/' . $feedback->fdid)}}" class="btn btn-primary">Mark as Viewed</a>
                    @endif

                </div>
            </div>
        </div>


        <div class="row-fluid">
            <div class="grid simple col-md-10 col-md-offset-1">
                <div class="grid-title no-border">
                    @if($feedback->isViewed == 1)
                        <label class="label label-success">Viewed</label>
                        by {{$feedback->User->fname}} {{$feedback->User->sname}}
                    @else
                        <label class="label label-primary">Not Viewed</label>
                    @endif


                </div>
                <div class="grid-body no-border">
                    <br>
                    <div class="row">
                        <p>
                            {{$feedback->content}}
                        </p>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-4">
                            <img src="{{$feedback->Patient->image}}" class="img img-thumbnail"/>
                        </div>
                        <div class="form-group col-md-4 col-sm-12">
                            <br>
                            <label class="form-label">Status</label>
                            @if($feedback->Patient->isConfirmed == 1)
                                <label class="label label-success">Confirmed</label>
                            @else
                                <label class="label label-danger">Un-Confirmed</label>
                            @endif
                            <br>
                            <label class="form-label">Name</label>
                            <span class="help">{{$feedback->Patient->fname}}</span>
                            <span class="help">{{$feedback->Patient->sname}}</span>
                            <br>
                            <label class="form-label">Email</label>
                            <span class="help">{{$feedback->Patient->email}}</span>
                            <br>
                            <label class="form-label">Phone</label>
                            <span class="help">{{$feedback->Patient->phone}}</span>
                            <br>
                            <label class="form-label">Gender</label>
                            <span class="help">{{$feedback->Patient->gender}}</span>

                        </div>

                        <div class="form-group col-md-4 col-sm-12">

                            <br>
                            <label class="form-label">DOB</label>
                            <span class="help">{{$feedback->Patient->dob}}</span>
                            <br>
                            <label class="form-label">Height</label>
                            <span class="help">{{$feedback->Patient->height}}cm</span>
                            <br>
                            <label class="form-label">Weight</label>
                            <span class="help">{{$feedback->Patient->weight}}kg</span>
                            <br>
                            <label class="form-label">Total Cases</label>
                            <span class="help">{{count($feedback->Patient->Cases)}}</span>
                            <br>

                        </div>
                    </div>

                </div>
            </div>

        </div>



    </div>
@endsection