@extends('layouts.admin')

@section('content')

    <div class="page-content">

        <div class="clearfix"></div>
        <div class="content sm-gutter">
            @include('notification')
            <div class="page-title">
                <a href="{{url('manage-hospitals')}}">
                    <i class="icon-custom-left"></i>
                </a>

                <div class="pull-right">
                    <a href="{{url('edit-hospital/' . $hospital->hid)}}" class="btn btn-warning">Edit</a>
                    @if($hospital->isPublished == 0)
                        <a href="{{url('publish-hospital/' . $hospital->hid)}}" class="btn btn-primary">Publish</a>
                    @else
                        <a href="{{url('unpublish-hospital/' . $hospital->hid)}}" class="btn btn-danger">Un-Publish</a>
                    @endif

                </div>
            </div>
        </div>


        <div class="row-fluid">
            <div class="grid simple col-md-10 col-md-offset-1">
                <div class="grid-title no-border">
                </div>
                <div class="grid-body no-border">
                    <br>
                    <div class="row">

                        <div class="col-md-12" style="margin-top: -30px;margin-bottom: 10px;">
                            <div class="col-md-1" style="padding:0;">
                                <img style="float: left; min-height: 52px; min-width: 52px" src="{{$hospital->User->image}}" alt="" class="img img-thumbnail"/>
                            </div>

                            <div class="col-md-11">
                                <h3 style="padding: 10px"><span class="semi-bold">{{$hospital->name}} </span></h3>
                            </div>

                        </div>

                        <div class="form-group col-md-6 col-sm-12">

                            <h4>Manager Details</h4>
                            <hr>
                            <label class="form-label">Name</label>
                            <span class="help">{{$hospital->User->fname}}</span>
                            <span class="help">{{$hospital->User->sname}}</span>
                            <br>
                            <label class="form-label">Email</label>
                            <span class="help">{{$hospital->User->email}}</span>
                            <br>
                            <label class="form-label">Phone</label>
                            <span class="help">{{$hospital->User->phone}}</span>
                            <br>
                            <label class="form-label">Address</label>
                            <span class="help">{{$hospital->User->address}}</span>
                            <br>
                            <label class="form-label">Created By</label>
                            <span class="help">{{$hospital->CreatedBy->fname}} {{$hospital->CreatedBy->sname}}</span>
                            <br>
                            <label class="form-label">Registered</label>
                            <span class="help">{{$hospital->created_at->toDayDateTimeString()}}</span>

                        </div>

                        <div class="form-group col-md-6 col-sm-12">
                            <h4>Hospital Details</h4>
                            <hr>
                            <label class="form-label">Status</label>
                            @if($hospital->isPublished == 1)
                                <label class="label label-success">Published</label>
                            @else
                                <label class="label label-danger">Not Published</label>
                            @endif
                            <br>

                            <label class="form-label">Hospital Name</label>
                            <span class="help">{{$hospital->name}}</span>

                            <br>
                            <label class="form-label">Hospital Phone</label>
                            <span class="help">{{$hospital->phone}}</span>

                            <br>
                            <label class="form-label">Hospital Email</label>
                            <span class="help">{{$hospital->email}}</span>

                            <br>
                            <label class="form-label">City</label>
                            <span class="help">{{$hospital->city}}</span>

                            <br>
                            <label class="form-label">Address</label>
                            <span class="help">{{$hospital->address}}</span>
                        </div>
                    </div>


                </div>
            </div>

        </div>

        <div class="row-fluid">
            <div class="grid simple col-md-10 col-md-offset-1">
                <div class="grid-title no-border">
                    <h4>Specializations</h4>
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                    <a href="#grid-config" data-toggle="modal" class="config"></a>
                    <a href="javascript:;" class="reload"></a>
                    <a href="javascript:;" class="remove"></a>
                </div>

                <div class="grid-body no-border">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Specialization</th>
                            <th>Description</th>
                            <th>Date Added</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>


                        <?php $hospitalSpecializations = $hospital->Specialization()->paginate(5); ?>
                        @foreach($hospitalSpecializations as $specialization)
                            <tr>
                                <td>{{$specialization->Specialization->name}}</td>
                                <td>{{$specialization->Specialization->description}}</td>
                                <td>{{$specialization->created_at->toDayDateTimeString()}}</td>
                                <td>
                                    <a href="{{url('remove-hospital-specialization/' . $hospital->hid . "/" .  $specialization->spid)}}">remove</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{$hospitalSpecializations->render()}}
                </div>

            </div>
        </div>

        <div class="row-fluid">
            <div class="grid simple col-md-10 col-md-offset-1">
                <div class="grid-title no-border">
                    <h4>Add Specializations</h4>
                </div>
                <div class="grid-body no-border">

                    @if(count($specializations) <= 0)
                        <p align="center">
                            Hospital already supports all available specialzations on the system
                        </p>

                    @else
                        <form method="post" action="{{url('add-hospital-specialization')}}">
                            {{csrf_field()}}
                            <input type="hidden" name="hid" value="{{$hospital->hid}}">

                            @foreach($specializations as $specialization)
                                <div class="form-group col-md-4" style="padding: 0; margin: 0;">
                                    <input class="col-md-1" type="checkbox" name="spid[]" value="{{$specialization->spid}}" >
                                    <label class="col-md-11">{{$specialization->name}}</label>
                                </div>
                            @endforeach

                            <div class="col-md-1 col-md-offset-11">
                                <button class="btn btn-primary">Add</button>
                            </div>
                        </form>

                    @endif


                </div>
            </div>
        </div>




    </div>
@endsection