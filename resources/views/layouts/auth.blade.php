<!DOCTYPE html>
<html>


<head lang="en">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Doctap</title>

    <link href="img/favicon.144x144.html" rel="apple-touch-icon" type="image/png" sizes="144x144">
    <link href="img/favicon.114x114.html" rel="apple-touch-icon" type="image/png" sizes="114x114">
    <link href="img/favicon.72x72.html" rel="apple-touch-icon" type="image/png" sizes="72x72">
    <link href="img/favicon.57x57.html" rel="apple-touch-icon" type="image/png">
    <link href="img/favicon.html" rel="icon" type="image/png">
    <link href="img/favicon-2.html" rel="shortcut icon">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>


    <![endif]-->
    <link rel="stylesheet" href="{{url('css/separate/pages/login.min.css')}}">
    <link rel="stylesheet" href="{{url('css/lib/font-awesome/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{url('css/lib/bootstrap/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('css/main.css')}}">
</head>
<body>

<style>
    body{
        background-image: url("assets/img/background.jpg");
        background-size: cover;
    }

    .link{
        color: white !important;
        text-decoration: underline;
        font-size: small;
    }

    .help-block {
        color:red;
        text-align: center;
    }
</style>

<div class="page-center">
    <div class="page-center-in">
        <div class="container-fluid">

            @yield('content')

            <div align="center" style="color: white;">
                <br><br>
                <p style="font-size: small">&copy;<?php echo date('Y'); ?> Innovious Limited. All Rights Reserved.</p>

                <a class="link" href="{{url('terms')}}">Terms Of Use</a> |  <a class="link" href="{{url('privacy')}}">Privacy Policy</a> |  <a class="link" href="{{url('contact')}}">Contact Us</a>
            </div>

        </div>
    </div>
</div><!--.page-center-->


<script src="{{url('js/lib/jquery/jquery.min.js')}}"></script>
<script src="{{url('js/lib/tether/tether.min.js')}}"></script>
<script src="{{url('js/lib/bootstrap/bootstrap.min.js')}}"></script>
<script src="{{url('js/plugins.js')}}"></script>
<script type="text/javascript" src="{{url('js/lib/match-height/jquery.matchHeight.min.js')}}"></script>
<script>
    $(function() {
        $('.page-center').matchHeight({
            target: $('html')
        });

        $(window).resize(function(){
            setTimeout(function(){
                $('.page-center').matchHeight({ remove: true });
                $('.page-center').matchHeight({
                    target: $('html')
                });
            },100);
        });
    });
</script>
<script src="{{url('js/app.js')}}"></script>
</body>

</html>
