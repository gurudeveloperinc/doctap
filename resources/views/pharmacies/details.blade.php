@extends('layouts.admin')

@section('content')

    <style>
        hr{
            margin-top: 0;
        }
    </style>

    <div class="page-content">

        <div class="clearfix"></div>
        <div class="content sm-gutter">
            @include('notification')
            <div class="page-title" style="margin-left: 80px;">
                <a href="{{url('manage-pharmacies')}}" >
                    <i class="icon-custom-left"></i>
                </a>

                <div class="pull-right">

                    @if($pharmacy->isSuspended == 0)
                        <a style="margin-right:20px !important;" href="{{url('suspend-pharmacy/' . $pharmacy->pharmid)}}" class="btn btn-danger">Suspend pharmacy</a>
                    @else
                        <a style="margin-right:20px !important;" href="{{url('unsuspend-pharmacy/' . $pharmacy->pharmid)}}" class="btn btn-warning">Readmit pharmacy</a>
                    @endif
                </div>
            </div>


            <div class="row-fluid">
                <div class="grid simple col-md-10 col-md-offset-1">
                    <div class="grid-title no-border">
                    </div>
                    <div class="grid-body no-border" >
                        <br>
                        <div class="row">

                            <div class="col-md-12" style="margin-top: -30px;margin-bottom: 10px;">
                                <div class="col-md-1" style="padding:0;">
                                    <img style="float: left;" src="{{$pharmacy->User->image}}" alt="" class="img img-thumbnail"/>
                                </div>

                                <div class="col-md-11">
                                    <h3 style="padding: 10px"><span class="semi-bold">{{$pharmacy->name}}</span></h3>
                                </div>

                            </div>

                            <div class="form-group col-md-6 col-sm-12">

                                <h4>Manager Details</h4>
                                <hr>
                                <label class="form-label">Name</label>
                                <span class="help">{{$pharmacy->User->fname}}</span>
                                <span class="help">{{$pharmacy->User->sname}}</span>
                                <br>
                                <label class="form-label">Email</label>
                                <span class="help">{{$pharmacy->User->email}}</span>
                                <br>
                                <label class="form-label">Phone</label>
                                <span class="help">{{$pharmacy->User->phone}}</span>
                                <br>
                                <label class="form-label">Address</label>
                                <span class="help">{{$pharmacy->User->address}}</span>
                                <br>
                                <label class="form-label">Created By</label>
                                <span class="help">{{$pharmacy->CreatedBy->fname}} {{$pharmacy->CreatedBy->sname}}</span>
                                <br>
                                <label class="form-label">Registered</label>
                                <span class="help">{{$pharmacy->created_at->toDayDateTimeString()}}</span>
                            </div>

                            <div class="form-group col-md-6 col-sm-12">
                                <h4>Pharmacy Details</h4>
                                <hr>
                                <label class="form-label">Pharmacy ID</label>
                                <span class="help">{{$pharmacy->pharmid}}</span>

                                <br>
                                <label class="form-label">Pharmacy Phone</label>
                                <span class="help">{{$pharmacy->phone}}</span>

                                <br>
                                <label class="form-label">Pharmacy Email</label>
                                <span class="help">{{$pharmacy->email}}</span>

                                <br>
                                <label class="form-label">City</label>
                                <span class="help">{{$pharmacy->city}}</span>

                                <br>
                                <label class="form-label">Address</label>
                                <span class="help">{{$pharmacy->address}}</span>
                            </div>
                        </div>



                    </div>
                </div>

            </div>

            <div class="row-fluid">
                <div class="grid simple col-md-10 col-md-offset-1">
                    <div class="grid-title no-border">
                        <h4>Cases</h4>
                    </div>
                    <div class="grid-body no-border">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Case ID</th>
                                <th>Date Started</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php $pharmacyCases = $pharmacy->Cases()->paginate(10);  ?>
                            @foreach($pharmacyCases as $case)
                                <tr>
                                    <td>{{$case->casid}}</td>
                                    <td>{{$case->created_at->toDayDateTimeString()}}</td>
                                    <td>
                                        @if($case->status == 'Pending')
                                            <label class="label label-warning">Pending</label>
                                        @endif
                                        @if($case->status == 'Complete')
                                            <label class="label label-success">Complete</label>
                                        @endif
                                        @if($case->status == 'Cancelled')
                                            <label class="label label-danger">Cancelled</label>
                                        @endif

                                    </td>
                                    <td>
                                        <a href="{{url('case/' . $case->casid)}}">View</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        {{$pharmacyCases->render()}}
                    </div>

                </div>
            </div>

        </div>
    </div>
@endsection