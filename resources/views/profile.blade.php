@extends('layouts.admin')

@section('content')

    <div class="page-content">

        <div class="clearfix"></div>
        <div class="content sm-gutter">
            @include('notification')
            <div class="page-title">

                <h3><span class="semi-bold">{{Auth::user()->fname}} </span></h3>

                <div class="pull-right">

                    {{--@if($lab->isBanned == 0)--}}
                    {{--<a href="{{url('ban-lab/' . $lab->docid)}}" class="btn btn-danger">Ban lab</a>--}}
                    {{--@else--}}
                    {{--<a href="{{url('unban-lab/' . $lab->docid)}}" class="btn btn-warning">Un-Ban lab</a>--}}
                    {{--@endif--}}
                    {{--</div>--}}
                </div>
            </div>


            <div class="row-fluid">
                <div class="grid simple col-md-10 col-md-offset-1">
                    <div class="grid-title no-border">
                    </div>
                    <div class="grid-body no-border">
                        <br>
                        <div class="row">

                            <div class="col-md-4">
                                <img src="{{$lab->User->image}}" alt="" class="img img-thumbnail"/>
                            </div>
                            <div class="form-group col-md-4 col-sm-12">

                                <h4>Account Details</h4>
                                <hr>
                                <label class="form-label">Name</label>
                                <span class="help">{{$lab->User->fname}}</span>
                                <span class="help">{{$lab->User->sname}}</span>
                                <br>
                                <label class="form-label">Email</label>
                                <span class="help">{{$lab->User->email}}</span>
                                <br>
                                <label class="form-label">Phone</label>
                                <span class="help">{{$lab->User->phone}}</span>
                                <br>
                                <label class="form-label">Address</label>
                                <span class="help">{{$lab->User->address}}</span>
                                <br>
                                <label class="form-label">Created By</label>
                                <span class="help">{{$lab->CreatedBy->fname}} {{$lab->CreatedBy->sname}}</span>

                            </div>

                            <div class="form-group col-md-4 col-sm-12">
                                <h4>Lab Details</h4>
                                <hr>
                                <label class="form-label">Lab Name</label>
                                <span class="help">{{$lab->name}}</span>

                                <br>
                                <label class="form-label">Lab Phone</label>
                                <span class="help">{{$lab->phone}}</span>

                                <br>
                                <label class="form-label">Lab Email</label>
                                <span class="help">{{$lab->email}}</span>

                                <br>
                                <label class="form-label">City</label>
                                <span class="help">{{$lab->city}}</span>

                                <br>
                                <label class="form-label">Address</label>
                                <span class="help">{{$lab->address}}</span>
                            </div>
                        </div>

                        <div class="row form-group col-md-offset-4">
                            <hr>
                            <h4>Payment Details</h4>
                            <br>
                            <label class="form-label">Payment Method</label>
                            <span class="help">{{$lab->paymentMethod}}</span>

                            @if($lab->paymentMethod == 'paypal')
                                <br>
                                <label class="form-label">Paypal Email</label>
                                <span class="help">{{$lab->paypal}}</span>
                            @else

                                <br>
                                <label class="form-label">Account Name</label>
                                <span class="help">{{$lab->accountName}}</span>

                                <br>
                                <label class="form-label">Account Number</label>
                                <span class="help">{{$lab->accountNumber}}</span>
                                <br>
                                <label class="form-label">Account Bank</label>
                                <span class="help">{{$lab->accountBank}}</span>

                            @endif

                        </div>

                    </div>
                </div>

            </div>

        </div>
    </div>

@endsection
