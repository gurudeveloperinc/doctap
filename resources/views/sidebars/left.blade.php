<?php use Illuminate\Support\Facades\URL; ?>
<div class="page-sidebar " id="main-menu">
    <!-- BEGIN MINI-PROFILE -->
    <div class="page-sidebar-wrapper scrollbar-dynamic" id="main-menu-wrapper">
        <div class="user-info-wrapper sm">
            {{--<div class="profile-wrapper sm">--}}
                {{--<img src="{{Auth::user()->image}}" alt="Profile Image"  width="69" height="69" />--}}
                {{--<div class="availability-bubble online"></div>--}}
            {{--</div>--}}
            {{--<div class="user-info sm">--}}
                {{--<div class="username">{{Auth::user()->fname}} <span class="semi-bold">{{Auth::user()->sname}}</span></div>--}}
                {{--@if(Auth::user()->role == 'Lab')--}}
                    {{--{{Auth::user()->Lab->name}}--}}
                {{--@endif--}}
                {{--@if(Auth::user()->role == 'Pharmacy')--}}
                    {{--{{Auth::user()->Pharmacy->name}}--}}
                {{--@endif--}}
                {{--@if(Auth::user()->role == 'Hospital')--}}
                    {{--{{Auth::user()->Hospital->name}}--}}
                {{--@endif--}}

                {{--<div class="status">{{Auth::user()->role}}</div>--}}
            {{--</div>--}}
        </div>
        <!-- END MINI-PROFILE -->
        <!-- BEGIN SIDEBAR MENU -->

        <ul style="margin-top: 20px;">
            <li class="start @if(URL::current() == url("/") ||
            URL::current() == url("admin")
            )open active @endif "> <a href="{{url('/')}}"><i class="fa fa-tachometer-alt"></i> <span class="title">Dashboard</span> <span class="selected"></span> </a>
            </li>


            @if(Auth::user()->role == "Lab")
                <li class="start @if(URL::current() == url("/dashboard/lab/cases"))open active @endif ">
                    <a href="javascript:;"> <i class="fa fa-notes-medical"></i> <span class="title">Cases</span> <span class=" arrow"></span> </a>
                    <ul class="sub-menu">
                        <li> <a href="{{url('dashboard/lab/cases')}}">All </a> </li>
                    </ul>
                </li>
            @endif

            @if(Auth::user()->role == 'Admin')
                <li class="start @if(URL::current() == url("manage-cases"))open active @endif ">
                    <a href="javascript:;"> <i class="fa fa-notes-medical"></i> <span class="title">Cases</span> <span class=" arrow"></span> </a>
                    <ul class="sub-menu">
                        <li> <a href="{{url('manage-cases')}}">All </a> </li>
                        <li> <a href="{{url('manage-cases?filter=available')}}">Available</a> </li>
                        <li> <a href="{{url('manage-cases?filter=pending')}}">Pending</a> </li>
                        <li> <a href="{{url('manage-cases?filter=completed')}}">Completed</a> </li>
                        <li> <a href="{{url('manage-cases?filter=cancelled')}}">Cancelled</a> </li>
                    </ul>
                </li>

                <li class="start @if(URL::current() == url("manage-doctors"))open active @endif ">
                    <a href="{{url('manage-doctors')}}"> <i class="fa fa-user-md"></i> <span class="title">Doctors</span> </a>
                </li>
                <li class="start @if(URL::current() == url("manage-patients"))open active @endif ">
                    <a href="{{url('manage-patients')}}"> <i class="fa fa-user"></i><span class="title"> Patients </span> </a>
                </li>

                <li class="start @if(URL::current() == url("feedback"))open active @endif ">
                    <a href="javascript:;"> <i class="fa fa-comment-alt"></i> <span class="title">Feedback</span> <span class=" arrow"></span> </a>
                    <ul class="sub-menu">
                        <li
                                class="start @if(URL::current() == url("feedback") )open active @endif "
                        > <a href="{{url('feedback')}}"> View Feedback</a> </li>
                    </ul>
                </li>

                <li class="start @if(URL::current() == url("add-lounge-item") || URL::current() == url("manage-lounge"))open active @endif ">
                    <a href="javascript:;"> <i class="fa fa-newspaper"></i> <span class="title">Lounge</span> <span class=" arrow"></span> </a>
                    <ul class="sub-menu">
                        <li
                                class="start @if(URL::current() == url("add-lounge-item") )open active @endif "
                        > <a href="{{url('add-lounge-item')}}">Add Article</a> </li>
                        <li
                                class="start @if(URL::current() == url("manage-lounge") )open active @endif "
                        > <a href="{{url('manage-lounge')}}">View Articles</a> </li>
                    </ul>
                </li>

            @endif


            @if(Auth::user()->role == 'Admin' || Auth::user()->role == 'Hospital')
                <li class="start @if(URL::current() == url("add-hospital") || URL::current() == url("manage-hospitals"))open active @endif ">
                    <a href="javascript:;"> <i class="fa fa-hospital"></i> <span class="title">Hospitals</span> <span class=" arrow"></span> </a>
                    <ul class="sub-menu">
                        <li
                                class="start @if(URL::current() == url("add-hospital") )open active @endif "
                        > <a href="{{url('add-hospital')}}">Add Hospitals</a> </li>
                        <li
                                class="start @if(URL::current() == url("manage-hospitals") )open active @endif "
                        > <a href="{{url('manage-hospitals')}}">Manage Hospitals</a> </li>
                    </ul>
                </li>
            @endif

            @if(Auth::user()->role == 'Admin')
                <li class="start @if(URL::current() == url("add-lab") || URL::current() == url("manage-labs"))open active @endif ">
                    <a href="javascript:;"> <i class="fa fa-flask"></i> <span class="title">Labs</span> <span class=" arrow"></span> </a>
                    <ul class="sub-menu">
                        <li
                                class="start @if(URL::current() == url("add-lab") )open active @endif "
                        > <a href="{{url('add-lab')}}"> Add Labs</a> </li>
                        <li
                                class="start @if(URL::current() == url("manage-labs") )open active @endif "
                        > <a href="{{url('manage-labs')}}">Manage Labs </a> </li>
                    </ul>
                </li>
            @endif

            @if(Auth::user()->role == 'Admin' || Auth::user()->role == 'Pharmacy')
                <li class="start @if(URL::current() == url("add-pharmacy") || URL::current() == url("manage-pharmacies"))open active @endif ">
                    <a href="javascript:;"> <i class="fa fa-pills"></i> <span class="title">Pharmacies</span> <span class=" arrow"></span> </a>
                    <ul class="sub-menu">
                        <li
                                class="start @if(URL::current() == url("add-pharmacy") )open active @endif "
                        > <a href="{{url('add-pharmacy')}}"> Add Pharmacies </a> </li>
                        <li
                                class="start @if(URL::current() == url("manage-pharmacies") )open active @endif "
                        > <a href="{{url('manage-pharmacies')}}"> Manage Pharmacies </a> </li>
                    </ul>
                </li>
            @endif


            @if(Auth::user()->role == 'Admin')


                <li class="start @if(URL::current() == url("manage-income") )open active @endif ">
                <a href="javascript:;"> <i class="fa fa-money-bill-alt"></i> <span class="title">Income</span> <span class=" arrow"></span> </a>
                <ul class="sub-menu">
                    <li> <a href="{{url('manage-income')}}">All </a> </li>
                    <li> <a href="{{url('manage-income?filter=bill')}}">Gross Bill</a> </li>
                    <li> <a href="{{url('manage-income?filter=income')}}">Gross Income</a> </li>
                    <li> <a href="{{url('manage-income?filter=defaulters')}}">Defaulters</a> </li>
                </ul>
                </li>

                <li class="start @if(URL::current() == url("manage-payments") )open active @endif ">
                    <a href="javascript:;"> <i class="fa fa-donate"></i> <span class="title">Payments</span> <span class=" arrow"></span> </a>
                    <ul class="sub-menu">
                        <li> <a href="{{url('manage-payments?filter=all')}}">All</a> </li>
                        <li> <a href="{{url('manage-payments?filter=due')}}">Due</a> </li>
                        <li> <a href="{{url('manage-payments?filter=completed')}}">Completed</a> </li>
                        <li> <a href="{{url('manage-payments?filter=pending')}}">Pending</a> </li>
                        <li> <a href="{{url('manage-payments?filter=paid')}}">Paid</a> </li>
                    </ul>
                </li>

                <li
                        class="start @if(URL::current() == url("manage-specializations") ||
                         URL::current() == url("manage-tests")||
                         URL::current() == url("manage-settings")||
                         URL::current() == url("manage-terms") ||
                         URL::current() == url("manage-privacy") ||
                         URL::current() == url("manage-faqs")
                         )open active @endif "
                >
                    <a href="javascript:;"> <i class="fa fa-cog"></i> <span class="title">Admin</span> <span class=" arrow"></span> </a>
                    <ul class="sub-menu">
                        <li
                                class="start @if(URL::current() == url("manage-tests") )open active @endif "
                        > <a href="{{url('manage-tests')}}">Manage Tests</a> </li>
                        <li
                                class="start @if(URL::current() == url("manage-specializations") )open active @endif "
                        > <a href="{{url('manage-specializations')}}">Manage Specializations</a> </li>
                        <li
                                class="start @if(URL::current() == url("manage-settings") )open active @endif "
                        > <a href="{{url('manage-settings')}}">Manage Settings</a> </li>
                        <li
                                class="start @if(URL::current() == url("manage-terms") )open active @endif "
                        > <a href="{{url('manage-terms')}}">Manage Terms</a> </li>
                        <li
                                class="start @if(URL::current() == url("manage-privacy") )open active @endif "
                        > <a href="{{url('manage-privacy')}}">Manage Privacy Policy</a> </li>
                        <li
                                class="start @if(URL::current() == url("manage-faqs") )open active @endif "
                        > <a href="{{url('manage-faqs')}}">Manage FAQs</a> </li>
                    </ul>
                </li>
            @endif

        </ul>

        <div class="clearfix"></div>
        <!-- END SIDEBAR MENU -->
    </div>
</div>