<?php use Carbon\Carbon; ?>
@extends('layouts.admin')

@section('content')

    <div class="page-content">

        <div class="clearfix"></div>
        <div class="content sm-gutter">
            @include('notification')
            <div class="page-title">
                <a href="{{url('manage-doctors')}}">
                    <i class="icon-custom-left"></i>
                </a>

                <div class="pull-right">
                    @if($doctor->isConfirmed == 0)
                        <a href="{{url('confirm-doctor/' . $doctor->docid)}}" class="btn btn-primary">Confirm Doctor</a>
                    @endif

                    @if($doctor->isSuspended == 0)
                        <a href="{{url('suspend-doctor/' . $doctor->docid)}}" class="btn btn-danger">Suspend Doctor</a>
                    @else
                        <a href="{{url('unsuspend-doctor/' . $doctor->docid)}}" class="btn btn-warning">Readmit Doctor</a>
                    @endif
                </div>
            </div>
        </div>


        <div class="row-fluid">
            <div class="grid simple col-md-10 col-md-offset-1">
                <div class="grid-title no-border">
                </div>
                <div class="grid-body no-border">
                    <br>
                    <div class="row">
                        <div class="col-md-3">


                            <form>
                                <input name="code" type="text" class="form-control">
                                <button>Submit</button>
                            </form>

                        </div>


                        <div class="col-md-12">
                            <div class=" col-md-3 col-md-offset-3">
                                <label class="form-label">Available Earnings</label>
                                <span class="help" style="font-size:16px;color:#098A54"><b>₦{{number_format($doctor->earnings,0)}}</b></span>
                            </div>
                            <div class=" col-md-3 ">
                                <label class="form-label">Pending Earnings</label>
                                <span class="help" style="font-size:16px;color:#098A54"><b>₦{{number_format($totalPendingMaturity,0)}}</b></span>
                            </div>

                            <div class=" col-md-3">
                                <label class="form-label">Total Earnings</label>
                                <span class="help" style="font-size:16px;color:#098A54"><b>₦{{number_format($doctor->totalEarnings,0)}}</b></span>

                            </div>

                        </div>

                    </div>
                </div>
            </div>

        </div>

    </div>



@endsection