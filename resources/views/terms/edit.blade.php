@extends('layouts.admin')

@section('content')

    <div class="page-content">

        <div class="clearfix"></div>
        <div class="content sm-gutter">
            @include('notification')
            <div class="row-fluid">

                <div class="span12">
                    <div class="grid simple ">
                        <div class="grid-title">
                            <h4>Edit <span class="semi-bold">Test</span></h4>
                            <div class="tools">
                                <a href="javascript:;" class="expand"></a>
                                <a href="#grid-config" data-toggle="modal" class="config"></a>
                                <a href="javascript:;" class="reload"></a>
                                <a href="javascript:;" class="remove"></a>
                            </div>
                        </div>
                        <div class="grid-body ">

                            <form method="post" action="{{url('edit-test')}}">
                                {{csrf_field()}}
                                <input type="hidden" name="testid" value="{{$test->testid}}">
                                <div class="form-group">
                                    <label>Title</label>
                                    <input type="text" class="form-control" value="{{$test->title}}" name="title">
                                </div>

                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea name="description" class="form-control">{{$test->description}}</textarea>
                                </div>

                                <button class="btn btn-primary">Save</button>

                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection